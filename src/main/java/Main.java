import entity.*;
import enums.PaymentOption;
import enums.ServiceName;
import enums.Type;
import repository.*;

import java.sql.Date;
import java.time.LocalDate;

public class Main {
//    public static void main(String[] args) {
//
////        OrderRepository orderRepository = new OrderRepository();
////
////        Order myOrder = new Order(Date.valueOf(LocalDate.now()), 150);
////
////        orderRepository.createOrder(myOrder);
////
////        Order orderToUpdate = new Order(1,Date.valueOf(LocalDate.now()),250);
////        orderRepository.updateOrder(orderToUpdate);
////
////        orderRepository.findOrderById("66");
//


//        ServiceRepository serviceRepository = new ServiceRepository();
//
//        Service service = new Service(ServiceName.PREMIUM);
//
//        serviceRepository.createService(service);
//
//
//        RoomType roomType = new RoomType(Type.PENTHOUSE,123);
//        RoomTypeRepository roomTypeRepository = new RoomTypeRepository();
//
//        roomTypeRepository.createRoomType(roomType);
//
//        Room room = new Room(2,100,3,4,roomType);
//        RoomRepository roomRepository = new RoomRepository();
//
//        roomRepository.createRoom(room);
//
//
//        User user = new User("Andrei", "Aaa", 45);
//
//        UserRepository userRepository = new UserRepository();
//
//        userRepository.createUser(user);
//
//        PaymentType paymentType = new PaymentType(PaymentOption.CASH);
//        PaymentTypeRepository paymentTypeRepository = new PaymentTypeRepository();
//
//        paymentTypeRepository.createPaymentType(paymentType);
//
//        Reservation reservation = new Reservation(user, Date.valueOf(LocalDate.now()),Date.valueOf(LocalDate.now().plusDays(5)),room,service,3,paymentType);
//
//        ReservationRepository reservationRepository = new ReservationRepository();
//
//        reservationRepository.createReservation(reservation);
//
//    }

    public static void main(String[] args) {
        Service service1 = new Service(ServiceName.PREMIUM);
        Service service2 = new Service(ServiceName.ALL_INCLUSIVE);
        Service service3 = new Service(ServiceName.BREAKFAST);
        Service service4 = new Service(ServiceName.DEFAULT);

        ServiceRepository serviceRepository1 = new ServiceRepository();
        ServiceRepository serviceRepository2 = new ServiceRepository();
        ServiceRepository serviceRepository3 = new ServiceRepository();
        ServiceRepository serviceRepository4 = new ServiceRepository();

        serviceRepository1.createService(service1);
        serviceRepository2.createService(service2);
        serviceRepository3.createService(service3);
        serviceRepository4.createService(service4);

        RoomType roomType1 = new RoomType(Type.PENTHOUSE, 30);
        RoomType roomType2 = new RoomType(Type.DOUBLE_KING_SIZE, 24);
        RoomType roomType3 = new RoomType(Type.DOUBLE_QUEENS_SIZE, 19);
        RoomType roomType4 = new RoomType(Type.DOUBLE_SINGLE, 12);
        RoomType roomType5 = new RoomType(Type.SINGLE, 8);
        RoomType roomType6 = new RoomType(Type.SUITE, 11);

        RoomTypeRepository roomTypeRepository1 = new RoomTypeRepository();
        RoomTypeRepository roomTypeRepository2 = new RoomTypeRepository();
        RoomTypeRepository roomTypeRepository3 = new RoomTypeRepository();
        RoomTypeRepository roomTypeRepository4 = new RoomTypeRepository();
        RoomTypeRepository roomTypeRepository5 = new RoomTypeRepository();
        RoomTypeRepository roomTypeRepository6 = new RoomTypeRepository();

        roomTypeRepository1.createRoomType(roomType1);
        roomTypeRepository2.createRoomType(roomType2);
        roomTypeRepository3.createRoomType(roomType3);
        roomTypeRepository4.createRoomType(roomType4);
        roomTypeRepository5.createRoomType(roomType5);
        roomTypeRepository6.createRoomType(roomType6);

        Room room1 = new Room(1, generatedRandomNumber(1, 10), 0, roomType1);
        Room room2 = new Room(1, generatedRandomNumber(10, 20), 0, roomType2);
        Room room3 = new Room(1, generatedRandomNumber(20, 30), 0, roomType3);
        Room room4 = new Room(1, generatedRandomNumber(30, 40), 0, roomType4);
        Room room5 = new Room(1, generatedRandomNumber(40, 50), 0, roomType5);
        Room room6 = new Room(1, generatedRandomNumber(50, 64), 0, roomType6);


        RoomRepository roomRepository1 = new RoomRepository();
        RoomRepository roomRepository2 = new RoomRepository();
        RoomRepository roomRepository3 = new RoomRepository();
        RoomRepository roomRepository4 = new RoomRepository();
        RoomRepository roomRepository5 = new RoomRepository();
        RoomRepository roomRepository6 = new RoomRepository();


        roomRepository1.createRoom(room1);
        roomRepository2.createRoom(room2);
        roomRepository3.createRoom(room3);
        roomRepository4.createRoom(room4);
        roomRepository5.createRoom(room5);
        roomRepository6.createRoom(room6);


        User user1 = new User("Andrei", "Mihalcea", 22);

        UserRepository userRepository = new UserRepository();

        userRepository.createUser(user1);

        PaymentType paymentType1 = new PaymentType(PaymentOption.CASH);
        PaymentType paymentType2 = new PaymentType(PaymentOption.CARD);
        PaymentType paymentType3 = new PaymentType(PaymentOption.VOUCHER);
        PaymentType paymentType4 = new PaymentType(PaymentOption.ONLINE);

        PaymentTypeRepository paymentTypeRepository1 = new PaymentTypeRepository();
        PaymentTypeRepository paymentTypeRepository2 = new PaymentTypeRepository();
        PaymentTypeRepository paymentTypeRepository3 = new PaymentTypeRepository();
        PaymentTypeRepository paymentTypeRepository4 = new PaymentTypeRepository();

        paymentTypeRepository1.createPaymentType(paymentType1);
        paymentTypeRepository2.createPaymentType(paymentType2);
        paymentTypeRepository3.createPaymentType(paymentType3);
        paymentTypeRepository4.createPaymentType(paymentType4);

        Reservation reservation = new Reservation(user1, Date.valueOf(LocalDate.now()),Date.valueOf(LocalDate.now().plusDays(6)),room2,service3,3,paymentType2);

        ReservationRepository reservationRepository = new ReservationRepository();

        reservationRepository.createReservation(reservation);

    }


    public static int generatedRandomNumber(int max, int min) {
        int result = (int) Math.floor(Math.random() * (max - min + 1) + min);
        return result;
    }
}
