import lombok.Data;


@Data
public class StorageSupport {

    private int extraBeds;

    public StorageSupport() {
        this.extraBeds = 100;
    }
}
