package entity;

import enums.PaymentOption;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
public class PaymentType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // TODO - to enum
    @Enumerated(EnumType.STRING)
    private PaymentOption paymentOption;

    @OneToMany(mappedBy = "paymentType")
    private List<Reservation> reservation;

    public PaymentType(PaymentOption paymentOption) {
        this.paymentOption = paymentOption;
    }
}
