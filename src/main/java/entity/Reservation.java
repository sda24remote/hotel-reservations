package entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;


@Entity
@NoArgsConstructor
@Data
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    @Temporal(TemporalType.DATE)
    private Date checkInDate;

    @Temporal(TemporalType.DATE)
    private Date checkOutDate;

    @ManyToOne
    @JoinColumn(name = "roomId")
    private Room room;

    @ManyToOne
    @JoinColumn(name = "serviceId")
    private Service serviceName;

    private int numberOfUsers;
    private  int totalPrice;

    @ManyToOne
    @JoinColumn(name = "paymentId")
    private PaymentType paymentType;

    public Reservation(User user, Date checkInDate, Date checkOutDate,
                       Room room, Service serviceName, int numberOfUsers,
                       PaymentType paymentType) {
        this.user = user;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
        this.room = room;
        this.serviceName = serviceName;
        this.numberOfUsers = numberOfUsers;
        this.paymentType = paymentType;
        this.totalPrice = getTotalPrice(); // TODO
    }

    public int getTotalPrice() { // TODO
        totalPrice += serviceName.getServiceName().getServicePrice();
        totalPrice += room.getRoomType().getPrice();
        return totalPrice;
    }
}
