package entity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    private int floor;
    private int price;
    private int roomNumber;
    private int numberOfExtraBeds;

    @ManyToOne
    @JoinColumn(name = "roomType")
    private RoomType roomType;

    @OneToMany(mappedBy = "room")
    private List<Reservation> reservation;

    public Room(int floor, int roomNumber, int numberOfExtraBeds, RoomType roomType) {
        this.floor = floor;
        this.price = roomType.getPrice();
        this.roomNumber = roomNumber;
        this.numberOfExtraBeds = numberOfExtraBeds;
        this.roomType = roomType;
    }

//    public void getExtraBeds(int numberOfExtraBeds) {
//        StorageSupport storage = new StorageSupport();
//        int result = storage.getExtraBeds();
//        if (numberOfExtraBeds < result){
//            result -= numberOfExtraBeds;
//            System.out.println("You can have your " + numberOfExtraBeds +" extra beds");
//        }else {
//            System.out.println("We are out of extra beds for this moment");
//        }
//
//    }
}

