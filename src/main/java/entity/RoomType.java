package entity;


import enums.Type;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
public class RoomType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    private Type type;

    private int price;


    @OneToMany(mappedBy = "roomType")
    private List<Room> rooms;

    public RoomType(Type type, int price) {
        this.type = type;
        this.price = price;
        this.price += type.getPrice(); // TODO
    }

}
