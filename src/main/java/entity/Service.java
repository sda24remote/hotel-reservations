package entity;

import enums.ServiceName;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    private ServiceName serviceName;


    @OneToMany(mappedBy = "serviceName")
    private List<Reservation> reservations = new ArrayList<>();

    public Service(ServiceName serviceName) {
        this.serviceName = serviceName;
    }
}
