package enums;

public enum PaymentOption {
    CARD,
    CASH,
    VOUCHER,
    ONLINE
}
