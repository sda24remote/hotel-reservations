package enums;



public enum ServiceName {

    ALL_INCLUSIVE(120),
    DEFAULT(65),
    BREAKFAST(85),
    PREMIUM(260);

    private int servicePrice;

    ServiceName(int servicePrice) {
        this.servicePrice = servicePrice;
    }

    public int getServicePrice() {
        return servicePrice;
    }


    public ServiceName setServicePrice(int servicePrice) {
        this.servicePrice = servicePrice;
        return this;
    }
}
