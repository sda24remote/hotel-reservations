package enums;

public enum Type {
    SINGLE(35,16),
    DOUBLE_SINGLE(50,10),
    DOUBLE_QUEENS_SIZE(75,9),
    DOUBLE_KING_SIZE(90,12),
    SUITE(45,14),
    PENTHOUSE(275,3)
    ;


    private int price;
    private int roomsAvailable;

    Type(int price,int roomsAvailable) {
        this.price = price;
        this.roomsAvailable = roomsAvailable;
    }

    public int getPrice() {
        return price;
    }
    public void decreaseRoomsAvailable(){
        roomsAvailable--;
    }

    public int getRoomsAvailable() {
        return roomsAvailable;
    }
}
