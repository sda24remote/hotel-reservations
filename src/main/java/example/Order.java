package example;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int orderId;

    @Column(name = "date")
    private Date date;

    @Column(name = "price")
    private float price;

    public Order(Date date, float price) {
        this.date = date;
        this.price = price;
    }

    public Order(int orderId, Date date, float price) {
        this.orderId = orderId;
        this.date = date;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", date=" + date +
                ", price=" + price +
                '}';
    }
}


/*
*
* C - createOrder (repository.Order order)
* R - findAll/findById/findByName etc
* U - updateOrder (repository.Order order)
* D - deleteOrder
* */