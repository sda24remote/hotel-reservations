package example;

import config.SessionTransaction;
import org.hibernate.query.Query;

import java.util.List;

public class OrderRepository extends SessionTransaction {

    public void createOrder(Order order) {
        openSession();
        session.save(order);

        System.out.println("Order saved successfully: \n" +  order);
        closeSession();
    }

    public void deleteOrder(Order order) {
        openSession();
        session.delete(order);
        closeSession();
    }

    public void updateOrder(Order order) {
        openSession();
        session.saveOrUpdate(order);

        System.out.println("Order updated successfully: \n" +  order);
        closeSession();
    }


    public List<Order> findOrderById(String id) {
        openSession();

        Query query = session.createQuery("FROM Order WHERE orderId = '" + id + "'");

        List<Order> orders = query.list();

        closeSession();
         return  orders;
    }
}
