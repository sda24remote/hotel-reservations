package repository;

import config.SessionTransaction;
import entity.PaymentType;
import org.hibernate.query.Query;

import java.util.List;

public class PaymentTypeRepository extends SessionTransaction {

    public void createPaymentType(PaymentType paymentType) {
        openSession();
        session.save(paymentType);

        System.out.println("PaymentType saved successfully: \n" + paymentType);
        closeSession();
    }

    public void deletePaymentType(PaymentType paymentType) {
        openSession();
        session.delete(paymentType);

        System.out.println("PaymentType deleted successfully: \n" + paymentType);
        closeSession();
    }

    public void updatePaymentType(PaymentType paymentType) {
        openSession();
        session.update(paymentType);

        System.out.println("PaymentType updated successfully: \n" + paymentType);
        closeSession();
    }

    public List<PaymentType> findByPayementTypeId(String id) {
        openSession();
        Query query = session.createQuery("FROM PaymentType where id = '" + id + "'");
        List<PaymentType> paymentTypeList = query.list();
        closeSession();
        return paymentTypeList;
    }
}
