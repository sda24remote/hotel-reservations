package repository;

import config.SessionTransaction;
import entity.Reservation;
import org.hibernate.query.Query;

import java.util.List;

public class ReservationRepository extends SessionTransaction {
    public void createReservation(Reservation reservation){
        openSession();
        session.save(reservation);

        reservation.getRoom().getRoomType().getType().decreaseRoomsAvailable();
        System.out.println(reservation.getRoom().getRoomType().getType().getRoomsAvailable());
        System.out.println("Reservation saved successfully: \n" + reservation);
        closeSession();
    }

    public void deleteReservation(Reservation reservation){
        openSession();
        session.delete(reservation);

        System.out.println("Reservation deleted successfully: \n" + reservation);
        closeSession();
    }

    public void updateReservation(Reservation reservation){
        openSession();
        session.update(reservation);

        System.out.println("Reservation updated successfully: \n" + reservation);
        closeSession();
    }

    public List<Reservation> findByReservationId(String id){
        openSession();
        Query query = session.createQuery("FROM Reservation where id = '" + id + "'");
        List<Reservation> result = query.list();
        closeSession();
        return result;
    }
}
