package repository;

import config.SessionTransaction;
import entity.Room;
import org.hibernate.query.Query;

import java.util.List;

public class RoomRepository extends SessionTransaction {

    public void createRoom(Room room){
    openSession();
    session.save(room);

    System.out.println("Room saved successfully: \n" + room);
    closeSession();
    }

    public void deleteRoom(Room room){
        openSession();
        session.delete(room);

        System.out.println("Room deleted successfully: \n" + room);
        closeSession();
    }

    public void updateRoom(Room room){
        openSession();
        session.update(room);

        System.out.println("Room updated successfully: \n" + room);
        closeSession();
    }

    public List<Room> findByRoomId(String roomId){
        openSession();
        Query query = session.createQuery("FROM Room WHERE id = '" + roomId +"'");
        List<Room> result = query.list();
        closeSession();
        return result;
    }
}
