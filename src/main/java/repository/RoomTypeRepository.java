package repository;

import config.SessionTransaction;
import entity.RoomType;
import org.hibernate.query.Query;

import java.util.List;

public class RoomTypeRepository extends SessionTransaction {

    public void createRoomType(RoomType roomType){
        openSession();
        session.save(roomType);

        System.out.println("Type saved successfully: \n" + roomType);
        closeSession();
    }

    public void deleteRoomType(RoomType roomType){
        openSession();
        session.delete(roomType);

        System.out.println("Type deleted successfully: \n" + roomType);
        closeSession();
    }

    public void updateRoomType(RoomType roomType){
        openSession();
        session.update(roomType);

        System.out.println("Type update successfully: \n" + roomType);
        closeSession();
    }

    public List<RoomType> findBYRoomTypeId(RoomType roomType){
        openSession();
        Query query = session.createQuery("FROM Type WHERE id = '" + roomType + "'");
        List<RoomType> roomTypes = query.list();
        closeSession();
        return roomTypes;
    }
}
