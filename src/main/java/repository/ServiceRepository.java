package repository;

import config.SessionTransaction;
import entity.Service;
import org.hibernate.query.Query;

import java.util.List;

public class ServiceRepository extends SessionTransaction {

    public void createService(Service service){
        openSession();
        session.save(service);
        System.out.println("Service saved successfully: \n" + service);
        closeSession();
    }

    public void deleteService(Service service){
        openSession();
        session.delete(service);
        System.out.println("Service deleted successfully: \n" + service);
        closeSession();
    }

    public void updateService(Service service){
        openSession();
        session.update(service);
        System.out.println("Service saved successfully: \n" + service);
        closeSession();
    }

    public List<Service> findByServiceId(String serviceId){
        openSession();
        Query query = session.createQuery("FROM Service WHERE id = '" + serviceId + "'");
        List<Service> services = query.list();
        closeSession();
        return services;
    }
}
