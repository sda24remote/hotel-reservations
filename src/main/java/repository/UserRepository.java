package repository;

import config.SessionTransaction;
import entity.User;
import org.hibernate.query.Query;

import java.util.List;


public class UserRepository extends SessionTransaction {


    public void createUser(User user){
        openSession();
        session.save(user);
        System.out.println("User saved successfully: \n" + user);
        closeSession();
    }

    public void deleteUser(User user){
        openSession();
        session.delete(user);
        System.out.println("User deleted successfully: \n" + user);
        closeSession();
    }

    public void updateUser(User user){
        openSession();
        session.update(user);
        System.out.println("User updated successfully: \n" + user);
        closeSession();
    }

    public List<User> findByUserId(String id){
        openSession();
        Query query = session.createQuery("FROM User WHERE id = '" + id +"'");
        List<User> users = query.list();
        closeSession();
        return  users;
    }

}
